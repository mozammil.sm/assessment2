package com.perfios.assessment;


import java.util.*;

public class Assessment {
    public static List<Player> createPlayer(List<Player> playerList) throws DuckExceptionClass{
        Scanner sc=new Scanner(System.in);
        int id;
        int matchPlayed;
        int totalRunsScored;
        int wicketsTaken;
        int outOnZeroScore;
        String name;
        String playerType;
        Validator ve=new Validator();

        System.out.println("enter details for player "+playerList.size()+1);
            id=playerList.size()+1;
            System.out.println("enter name");
            name=sc.next();
            System.out.println("enter match played");
            matchPlayed=sc.nextInt();
            System.out.println("enter total runs scored");
            totalRunsScored=sc.nextInt();
            System.out.println("enter wickets taken");
            wicketsTaken=sc.nextInt();
            System.out.println("enter out on zero");
            outOnZeroScore=sc.nextInt();
            System.out.println("enter player type");
            playerType=sc.next();
            int average=totalRunsScored/matchPlayed;
            System.out.println(average);
            Player p=new Player( id, name, playerType, average,totalRunsScored,matchPlayed, wicketsTaken, outOnZeroScore);
            ve.checkDuckException(outOnZeroScore,matchPlayed);
            playerList.add(p);
        return playerList;
    }
    public static void createFinalTeam(List<Player> playerList) throws LessWicketKeeperException, LessBowlerException {
        Validator ve=new Validator();
        String w="wicketkeeper";
        String bow="bowler";
        String bat="batsman";
        Map<Player,Integer> allPlayers=new HashMap<>();
        Iterator<Player> itr1 = playerList.listIterator();
        while (itr1.hasNext()) {
            Player p = itr1.next();
            allPlayers.put(p, p.getAverage());
        }
        // Sort the list
        LinkedHashMap<Player, Integer> sortedPlayers = new LinkedHashMap<>();

        allPlayers.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> sortedPlayers.put(x.getKey(), x.getValue()));

        LinkedHashMap<Player, Integer> playingEleven = new LinkedHashMap<>();
        int countBowlers=0;
        int countWicketKeeper=0;
        int totalPlayerCount=0;
        Iterator hmIterator = sortedPlayers.entrySet().iterator();

        while(hmIterator.hasNext() && totalPlayerCount<=11)
        {

            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            Player key= (Player) mapElement.getKey();
            System.out.println(key.wicketsTaken);
            if(totalPlayerCount<7) {
                if (key.getPlayerType().equalsIgnoreCase(bat)) {
                    playingEleven.put(key, key.getAverage());
                    totalPlayerCount++;
                } else if (key.getPlayerType().equalsIgnoreCase(bow)) {
                    playingEleven.put(key, key.getAverage());
                    countBowlers++;
                    totalPlayerCount++;
                } else if (key.getPlayerType().equalsIgnoreCase(w) && countWicketKeeper < 1) {
                    playingEleven.put(key, key.getAverage());
                    countWicketKeeper++;
                    totalPlayerCount++;
                }
            }
            else if(countWicketKeeper==0 && key.getPlayerType().equalsIgnoreCase(w)){
                    countWicketKeeper++;
                    totalPlayerCount++;
                    playingEleven.put(key,key.getAverage());
            }
            else if(countBowlers<3 && key.getPlayerType().equalsIgnoreCase(bow)){
                countBowlers++;
                totalPlayerCount++;
                playingEleven.put(key,key.getAverage());
            }
            else if(countBowlers<3 && countWicketKeeper==0 && (key.getPlayerType().equalsIgnoreCase(w) || key.getPlayerType().equalsIgnoreCase(bow)))
            {
                totalPlayerCount++;
                playingEleven.put(key, key.getAverage());
                if(key.getPlayerType().equalsIgnoreCase(w))
                    countWicketKeeper++;
                if(key.getPlayerType().equals(bow))
                    countBowlers++;
            }
            if(countBowlers>=3 && countWicketKeeper>=1){
                playingEleven.put(key, key.getAverage());
                totalPlayerCount++;
            }
        }
        ArrayList<Player> finalList=new ArrayList<>();
        for(Player key:playingEleven.keySet())
        {

            System.out.println(key.name);
            finalList.add(key);
        }
        ve.finalListException(finalList);
    }
    public static void update(String searchName,List<Player>playerList)throws DuckExceptionClass
    {
        Scanner sc=new Scanner(System.in);
        Validator ve=new Validator();
        Iterator<Player> itr = playerList.listIterator();
        while (itr.hasNext()) {
            Player p = itr.next();
            if (p.getName().equalsIgnoreCase(searchName))
            {
                System.out.println("update details for "+p.getName());
                System.out.println("enter matches played");
                p.setMatchPlayed(sc.nextInt());
                System.out.println("enter runs scored");
                p.setTotalRunsScored(sc.nextInt());
                System.out.println("enter wickets taken");
                p.setWicketsTaken(sc.nextInt());
                System.out.println("enter ducks");
                p.setWicketsTaken(sc.nextInt());
                ve.checkDuckException(p.outOnZeroScore,p.matchPlayed);
            }
        }
    }
    public static void delete(List<Player>playerList,String removedPlayer)
    {
        Iterator<Player> itr2 = playerList.listIterator();
        while (itr2.hasNext()) {
            Player p = itr2.next();
            if (p.getName().equalsIgnoreCase(removedPlayer)) {
                playerList.remove(p);
            }
        }
    }
    public static void displayPLayerList(List<Player> playerList)
    {
        if(playerList.isEmpty())
        {
            System.out.println("Empty Player List");
        }
        else {
            System.out.println("Id  Name          Match Played  Runs Scored    Wickets   Ducks    Player Type");
            Iterator<Player> itr = playerList.listIterator();
            while (itr.hasNext()) {
                Player p = itr.next();
                System.out.println(p.getId() + "   " + p.getName() + "         " + p.getMatchPlayed() + "           " + p.getTotalRunsScored() + "          " + p.getWicketsTaken() + "         " + p.getOutOnZeroScore() + "          " + p.getPlayerType());
            }
        }
    }
    public static void main(String[] args) throws LessBowlerException, LessWicketKeeperException, PlayerNotFoundException, DuckExceptionClass {
        Validator ve = new Validator();
        Scanner sc = new Scanner(System.in);
        ArrayList<Player> playerList = new ArrayList<>();
        int ch=1;
        String batsman="Batsman";
        String bowler="Bowler";
        Player player1 = new Player(1,"Shikhar",batsman,100,70500,400,25,32);
        Player player2 = new Player(2,"ViratKi",batsman,20,59500,400,25,32);
        Player player3 = new Player(3,"JadejaR","All-Rounder",90,10500,400,25,32);
        Player player4 = new Player(4,"M Shami",bowler,80,15500,400,25,32);
        Player player5 = new Player(5,"MPandey",bowler,70,13500,400,25,32);
        Player player6 = new Player(6,"JBumrah",bowler,80,38500,400,25,32);
        Player player7 = new Player(7,"Mayank",batsman,60,29500,400,25,32);
        Player player8 = new Player(8,"Shreyas",batsman,50,91500,400,25,32);
        Player player9 = new Player(9,"YChahal",bowler,40,85500,400,25,32);
        Player player10 = new Player(10,"K Rahul","WicketKeeper",40,75500,400,25,32);
        Player player11 = new Player(11,"Pandya",batsman,90,92500,400,25,32);
        Player player12 = new Player(12,"K Yadav",bowler,80,16500,400,25,32);
        Player player13 = new Player(13,"Thakur",bowler,20,7500,400,25,32);
        Player player14 = new Player(14,"N Saini",bowler,40,8500,400,25,32);
        Player player15 = new Player(15,"Shubman",batsman,70,72500,400,25,32);
        Player player16 = new Player(16,"Rahane",batsman,90,62500,400,25,32);
        Player player17 = new Player(17,"Ashwin",bowler,50,22500,400,25,32);
        Player player18 = new Player(18,"U Yadav",bowler,60,32500,400,25,32);
        Player player19 = new Player(19,"MSDhoni","WicketKeeper",30,98500,400,25,32);
        Player player20 = new Player(20,"H Vihari",batsman,30,52500,400,25,32);
        playerList.add(player1);
        playerList.add(player2);
        playerList.add(player3);
        playerList.add(player4);
        playerList.add(player5);
        playerList.add(player6);
        playerList.add(player7);
        playerList.add(player8);
        playerList.add(player9);
        playerList.add(player10);
        playerList.add(player11);
        playerList.add(player12);
        playerList.add(player13);
        playerList.add(player14);
        playerList.add(player15);
        playerList.add(player16);
        playerList.add(player17);
        playerList.add(player18);
        playerList.add(player19);
        playerList.add(player20);


        while(ch!=6){
            System.out.println("1: display all players 2:update player information by name 3:Display final team 4:Add new player 5:Delete Player by name 6:exit");
            ch = sc.nextInt();
            switch (ch) {
                case 1:
                    displayPLayerList(playerList);
                    break;
                case 2:
                    System.out.println("enter name of player to be updated");
                    sc.nextLine();
                    String searchName = sc.nextLine();
                    int found = ve.checkPlayer(searchName, playerList);
                    if (found == 1) {
                        update(searchName, playerList);
                        System.out.println("player updated");
                    }
                    break;
                case 3:
                    createFinalTeam(playerList);
                    break;
                case 4:
                    System.out.println("Add new player");
                    createPlayer(playerList);
                    break;
                case 5:
                    System.out.println("enter name of player to be removed");
                    sc.nextLine();
                    String removedPlayer = sc.nextLine();
                    int foundRemove = ve.checkPlayer(removedPlayer, playerList);
                    if (foundRemove == 1) {
                        System.out.println("player updated");
                        delete(playerList,removedPlayer);
                    }
                    break;
                case 6:
                    System.exit(0);
                    break;
                default:
                    System.out.println("wrong choice");
                    break;
            }
        }
    }
}
